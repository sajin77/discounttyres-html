<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>

<main>
   <section class="orderConfirmation_page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="order_box">
                    <div class="ordered_item">
                        <div class="item_img">
                           <img src="assets/images/itemImg.png" alt="">
                        </div>
                        <div class="item_details">
                            <h2>MDR Autos Centres</h2>
                            <p>
                                114B Manchester Road
ASHTON-UNDER-LYNE Lancashire OL5 9AY
                            </p>

                            <button>
                                View Location
                            </button>

                        </div>
                    </div>
                    <div class="order_price">
                        <h4>
                            Fitting Time
                        </h4>
                        <p>
                            Wed 3rd January 2024
08:00 - 09:00
                        </p>
                    </div>
                </div>
                <div class="customer_detailForm">
                    <form action="">
                        <h2>
                            Enter Your Details
                        </h2>
                        <div class="row personal_detail">
                            <div class="col-12 col-sm-6 col-lg-4">
                                <input type="text" placeholder="Firstname">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4">
                                <input type="text" placeholder="Lastname">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4">
                                <input type="text" placeholder="Email Address">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4">
                                <input type="text" placeholder="Telephone Number">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4">
                                <input type="text" placeholder="House Number or Name">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 col-xl-2">
                                <input type="text" placeholder="Car Registration">
                            </div>
                            <div class="col-12 col-sm-6 col-lg-4 col-xl-2">
                                <input type="text" placeholder="Car Mileage">
                            </div>
                        </div>
                        <div class="address_details">
                            <h2>
                                Cathedral Approach, Manchester, M1 1AD <span>Not your address?</span>
                            </h2>
                            <div class="row personal_detail">
                                <div class="col-12 col-sm-6">
                                    <input type="text" placeholder="Cathedral Approach">
                                </div>
                                <div class="col-12 col-sm-6">
                                    <input type="text" placeholder="Address 2">
                                </div>
                                <div class="col-12 col-sm-6">
                                    <input type="text" placeholder="Manchester">
                                </div>
                                <div class="col-12 col-sm-6">
                                    <input type="text" placeholder="M1 1AD">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="paymenthod_methods">
                    
                    <div class="row">
                        <div class="col-12">
                            <h2>
                                Confirm Your Order
                            </h2>
                            <div class="confirm_order">
                               
                                <div class="ordered_boxParent">
                                    <div class="ordered_item">
                                        <div class="about_product">
                                            <div class="product_img">
                                                <img src="assets/images/productimg.png" alt="">
                                            </div>
                                            <div class="product_details">
                                                <h6>
                                                    Bridgestone
                                                </h6>
                                                <h5>
                                                    225/45R17 91V
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="quality_box">
                                            <h6>
                                                Qty: <span>1</span>
                                            </h6>
                                        </div>
                                        <div class="price_box">
                                            <h6>
                                                Price: <span>£98.34</span>
                                            </h6>
                                        </div>
                                    </div>
                                    <div class="ordered_item">
                                        <div class="about_product">
                                            <div class="product_img">
                                                <img src="assets/images/productimg.png" alt="">
                                            </div>
                                            <div class="product_details">
                                                <h6>
                                                    Pirelli
                                                </h6>
                                                <h5>
                                                    225/45R17 91V
                                                </h5>
                                            </div>
                                        </div>
                                        <div class="quality_box">
                                            <h6>
                                                Qty: <span>1</span>
                                            </h6>
                                        </div>
                                        <div class="price_box">
                                            <h6>
                                                Price: <span>£98.34</span>
                                            </h6>
                                        </div>
                                    </div>
                                </div>
                                <div class="payment_options">
                                    <div class="price_detailBox">
                                        <h3>
                                            Price Details <span> (2 Item)</span>
                                        </h3>
                                        <div class="sub_total">
                                            <h6>
                                                Subtotal 
                                            </h6>
                                            <h5>
                                                £98.34 
                                            </h5>
                                        </div>
                                        <div class="sub_total">
                                            <h6>
                                                Subtotal 
                                            </h6>
                                            <h5>
                                                £98.34 
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="terms">
                                        <div class="check_boxterms">
                                            <div class="terms_point">
                                                <input type="checkbox" name="" id="">
                                            <h6>
                                                I Accept the Terms and conditions
                                            </h6>
                                            </div>
                                            <div class="terms_point">
                                                <input type="checkbox" name="" id="">
                                            <h6>
                                                I Accept the Privacy & Policy
                                            </h6>
                                            </div>
                                        </div>
                                        <button>
                                            Pay Now
                                        </button>
                                    </div>

                                    <ul class="cards">
                                        <li>
                                            <img src="assets/images/upicard1.png" alt="">
                                        </li>
                                        <li>
                                            <img src="assets/images/upicard2.png" alt="">
                                        </li>
                                        <li>
                                            <img src="assets/images/upicard3.png" alt="">
                                        </li>
                                        <li>
                                            <img src="assets/images/upicard4.png" alt="">
                                        </li>
                                        <li>
                                            <img src="assets/images/upicard2.png" alt="">
                                        </li>
                                    </ul>

                                </div>
                            </div>
                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   </section>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
