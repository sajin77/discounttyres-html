<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>

<main>
   <section class="booking_summarypage">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="booking_status">
                    <div class="confirmed_img">
                        <img src="assets/images/error.png" alt="">
                    </div>
                    <h2>
                        Booking Confirmed
                    </h2>
                    <p>
                        Thankyou for ordering your tyres with Discount Tyres
                    </p>
                    <h4>
                        Reference ID: <span> 2345684</span>
                    </h4>
                </div>
                <div class="booking_summaryDetails">
                    <div class="booking_centerBox">
                        <div class="item">
                            <h2>
                                Fitting Center
                            </h2>
                            <p>
                                MDR autos Centres
114B Manchester Road
ASHTON-UNDER-LYNE Lancashire OL5 9AY
                            </p>
                        </div>
                        <div class="item">
                            <h2>
                                Fitting Time
                            </h2>
                            <p>
                                Wed 3rd January 2024
                            </p>
                            <h6>
                                08:00 - 09:00
                            </h6>
                        </div>
                    </div>
                    <div class="bookeditem_details">
                        <h2>
                            Your Tyres
                        </h2>
                        <div class="confirmed_items">
                            <div class="ordered_boxParent">
                                <div class="ordered_item">
                                    <div class="about_product">
                                        <div class="product_img">
                                            <img src="assets/images/productimg.png" alt="">
                                        </div>
                                        <div class="product_details">
                                            <h6>
                                                Bridgestone
                                            </h6>
                                            <h5>
                                                225/45R17 91V
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="quality_box">
                                        <h6>
                                            Qty: <span>1</span>
                                        </h6>
                                    </div>
                                    <div class="price_box">
                                        <h6>
                                            Price: <span>£98.34</span>
                                        </h6>
                                    </div>
                                </div>
                                <div class="ordered_item">
                                    <div class="about_product">
                                        <div class="product_img">
                                            <img src="assets/images/productimg.png" alt="">
                                        </div>
                                        <div class="product_details">
                                            <h6>
                                                Pirelli
                                            </h6>
                                            <h5>
                                                225/45R17 91V
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="quality_box">
                                        <h6>
                                            Qty: <span>1</span>
                                        </h6>
                                    </div>
                                    <div class="price_box">
                                        <h6>
                                            Price: <span>£98.34</span>
                                        </h6>
                                    </div>
                                </div>
                                <div class="total_amount">
                                    Total  <span>£196.68</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   </section>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
