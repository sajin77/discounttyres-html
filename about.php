<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>


<main>
    <div class="about_page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact_details">
                      <div class="about_contents">
                        <h2>
                            <span>About</span> Us
                        </h2>
                        <p>
                            Discount Tyres online tyre store is the best place to come for your new tyres. We have premium branded tyres, fantastic performing mid-range brands and a wide selection of cheap car tyres from our budget range. We supply tyres from leading brands like Bridgestone, 
and many more, so you will be spoilt for choice. Don’t worry about how you’ll get your tyres fitted either, etyres have been providing convenient mobile tyre fitting for over 25 years.
                        </p>
                      </div>
                      <div class="about_image">
                        <img src="assets/images/about_image.png" alt="">
                      </div>
                       
                      
                    </div>
                   
                </div>
            </div>
        </div>
        <div class="outlets_countparent">
            <div class="container">
                <ul class="outlets_count">
                    <li>
                      <div class="outlet_units">
                        <div class="counter">
                          <h4 class="count">25</h4>
                          <span>+</span>
                        </div>
                        <h5>Years Experience</h5>
                      </div>
                    </li>
                    <li>
                      <div class="outlet_units">
                        <div class="counter">
                          <h4 class="count">500</h4>
                          <span>+</span>
                        </div>
                        <h5>Satisfied Clients</h5>
                      </div>
                    </li>
                    <li>
                      <div class="outlet_units">
                        <div class="counter">
                          <h4 class="count">37</h4>
                          <span>+</span>
                        </div>
                        <h5>Various Brands</h5>
                      </div>
                    </li>
                    <li>
                      <div class="outlet_units">
                        <div class="counter">
                          <h4 class="count">13</h4>
                          <span>+</span>
                        </div>
                        <h5>Outlets in UK</h5>
                      </div>
                    </li>
                  </ul>
            </div>
        </div>
      <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="vision_boxes">
                    <li>
                        <div class="vision_head">
                            <img src="assets/images/mission.svg" alt="">
                            <h2>
                                OUR MISSION
                            </h2>
                        </div>
                        <div class="vision_para">
                            <p>
                                Our mission is to ensure that every motorist has access to high-quality, affordable, and convenient tyre solutions. We strive to provide a seamless experience through our 24/7 availability, mobile tyre services, and a well-equipped tyres garage near you. Our dedication extends to offering a vast selection of tyre options to cater to all budgets and preferences while ensuring the utmost safety and performance on the road.
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="vision_head">
                            <img src="assets/images/Vision.svg" alt="">
                            <h2>
                                OUR VISION
                            </h2>
                        </div>
                        <div class="vision_para">
                           <p>
                            Our vision is to be the premier choice for tyre services in Liverpool and beyond, known for our unwavering commitment to customer satisfaction, quality, and innovation. We envisage a future where our mobile tyre services set the industry standard for convenience, reliability, and excellence, making tyre maintenance and replacement a stress-free task for all motorists.
                           </p>
                        </div>
                    </li>
                </ul>

                <div class="why_choose_section">
                    <h2>
                        <span>Why</span> Choose Us
                    </h2>
                    <ul class="why_choose">
                        <li>
                            <img src="assets/images/Price Tag.svg" alt="">
                            <span>
                                Competitive Price
                            </span>
                        </li>
                        <li>
                            <img src="assets/images/delivery (1).svg" alt="">
                            <span>
                                Free Delivery
                            </span>
                        </li>
                        <li>
                            <img src="assets/images/service.svg" alt="">
                            <span>
                                Same Day Service 
                            </span>
                        </li>
                    </ul>
                </div>

                <div class="our_history_section">
                    <h2>
                        <span>Our</span> History
                    </h2>
                    <p>
                        Discount Tyres online tyre store is the best place to come for your new tyres. We have premium branded tyres, fantastic performing mid-range brands and a wide selection of cheap car tyres from our budget range. We supply tyres from leading brands like Bridgestone, 
and many more, so you will be spoilt for choice. Don’t worry about how you’ll get your tyres fitted either, etyres have been providing convenient mobile tyre fitting for over 25 years.
Discount Tyres online tyre store is the best place to come for your new tyres. We have premium branded tyres, fantastic performing mid-range brands and a wide selection of cheap car tyres from our budget range. We supply tyres from leading brands like Bridgestone, 
and many more, so you will be spoilt for choice. Don’t worry about how you’ll get your tyres fitted either, etyres have been providing convenient mobile tyre fitting for over 25 years.
Discount Tyres online tyre store is the best place to come for your new tyres. We have premium branded tyres, fantastic performing mid-range brands and a wide selection of cheap car tyres from our budget range. We supply tyres from leading brands like Bridgestone, 
and many more, so you will be spoilt for choice. Don’t worry about how you’ll get your tyres fitted either, etyres have been providing convenient mobile tyre fitting for over 25 years.
                    </p>
                </div>

             
            </div>
        </div>
      </div>
      <div class="testimonials_slider">
        <h2>
            Testimonials
        </h2>
        <div class="swiper mySwiper testimonialswiper">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="testimonial_box">
                    <div class="fixed_profileimg">
                        <img src="assets/images/profileimg.png" alt="">
                    </div>
                    <h4>
                        Rohit Tonapi
                    </h4>
                    <p>
                        "Good to have a store which genuinely cares for its customers and makes sure they deliver in time and at reasonable rates! Keep up the good work! Looking forward to buying many more sets of tyres and putting on many more miles on the car and bike!"
                    </p>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial_box">
                    <div class="fixed_profileimg">
                        <img src="assets/images/profileimg.png" alt="">
                    </div>
                    <h4>
                        Rohit Tonapi
                    </h4>
                    <p>
                        "Good to have a store which genuinely cares for its customers and makes sure they deliver in time and at reasonable rates! Keep up the good work! Looking forward to buying many more sets of tyres and putting on many more miles on the car and bike!"
                    </p>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial_box">
                    <div class="fixed_profileimg">
                        <img src="assets/images/profileimg.png" alt="">
                    </div>
                    <h4>
                        Rohit Tonapi
                    </h4>
                    <p>
                        "Good to have a store which genuinely cares for its customers and makes sure they deliver in time and at reasonable rates! Keep up the good work! Looking forward to buying many more sets of tyres and putting on many more miles on the car and bike!"
                    </p>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial_box">
                    <div class="fixed_profileimg">
                        <img src="assets/images/profileimg.png" alt="">
                    </div>
                    <h4>
                        Rohit Tonapi
                    </h4>
                    <p>
                        "Good to have a store which genuinely cares for its customers and makes sure they deliver in time and at reasonable rates! Keep up the good work! Looking forward to buying many more sets of tyres and putting on many more miles on the car and bike!"
                    </p>
                </div>
              </div>
            </div>
            <div class="swiper-pagination" id="testimonial_dots"></div>
          </div>
      </div>
    </div>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
