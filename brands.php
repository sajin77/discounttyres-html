<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>

<main>
    <div class="banner_section">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="bannerslide_parent">
                <div class="banner_filterBox">
                  <h2>
                    Start by giving your register number
                  </h2>
                  <div class="reg_boxParent">
                    <div class="reg_box">
                      <div class="gb_blueBox">
                        <div class="round_gb">
                        <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
    <circle id="Ellipse 14" cx="6.5" cy="6.5" r="6" stroke="#FFCD02" stroke-dasharray="2 2"/>
    </svg>
    <h6>
    gb
    </h6>
    
                        </div>
                      </div>
                      <input type="text" placeholder="ENTER REG">
                    </div>
                    <div class="reg_searchBtn">
                      <button>
                        SEARCH BY REG
                      </button>
                    </div>
                  </div>
                  <div class="fully_fittedRadio_switch">
                    <div class="radio_switch">
                      <input type="radio" name="fully_fitted" id="">
                      <label for="">Fully fitted</label>
                    </div>
                    <div class="radio_switch">
                      <input type="radio" name="fully_fitted" id="">
                      <label for="">Mail Order</label>
                    </div>
                  </div>
                  <div class="or">
                    <span>Or</span>
                  </div>
                  <h2>
                    Check your Tyre size below
                  </h2>
                  <div class="tyre_sizes">
                    <select name="" id="">
                        <option value="">width</option>
                        <option value="">width</option>
                        <option value="">width</option>
                    </select>
                    <select name="" id="">
                      <option value="">Profile</option>
                      <option value="">Profile</option>
                    </select>
                    <select name="" id="">
                      <option value="">Wheel Size</option>
                      <option value="">Wheel Size</option>
                    </select>
                    <select name="" id="">
                      <option value="">Speed</option>
                      <option value="">Speed</option>
                    </select>
                  </div>
                  <div class="fully_fittedRadio_switch mt_19">
                    <div class="radio_switch">
                      <input type="radio" name="fully_fitted" id="">
                      <label for="">Fully fitted</label>
                    </div>
                    <div class="radio_switch">
                      <input type="radio" name="fully_fitted" id="">
                      <label for="">Mail Order</label>
                    </div>
                  </div>
                  <div class="continue_btn">
                    <button>
                     Continue
                    </button>
                  </div>
                </div>
              <div class="swiper mySwiper banner_slider">
                <div class="swiper-wrapper">
                  <div class="swiper-slide">
                    <div class="mrf_banner">
                     <div class="row banner_row">
                      <div class="col-12 col-md-12">
                    
                      </div>
                      <div class="col-12 col-xl-6">
                        <div class="about_mrf_tyres">
                          <div class="mrf_logo">
                            <img src="assets/images/icons/mrf.svg" alt="">
                          </div>
                          <h2>
                            Longer life Tyres
      for your Bikes
                          </h2>
                          <div class="know_moreBtn">
                            <button>
                              Know More
                            </button>
                          </div>
                        </div>
                      </div>
                     </div>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="mrf_banner2">
                      <div class="row banner_row">
                       <div class="col-12 col-md-6">
                         
                       </div>
                       <div class="col-12 col-lg-12">
                         <div class="banner_tyrespecs">
                            <div class="tyre_img">
                              <img src="assets/images/bannertyre.png" alt="">
                              <div class="curved_arrowgroup">
                                <img src="assets/images/icons/arrowgroup.png" alt="">
                                <div class="arow_tyresize left_tyre_size ">
                                  <h6>
                                    Width
                                  </h6>
                                  <h5>
                                    95
                                  </h5>
                                </div>
    
                                <div class="arow_tyresize top_tyre_size1">
                                  <h6>
                                    Profile
                                  </h6>
                                  <h5>
                                    195
                                  </h5>
                                </div>
                                <div class="arow_tyresize top_tyre_size2">
                                  <h6>
                                    Profile
                                  </h6>
                                  <h5>
                                    195
                                  </h5>
                                </div>
    
    
                                <div class="arow_tyresize right_tyre_size">
                                  <h6>
                                    Speed
                                  </h6>
                                  <h5>
                                    v
                                  </h5>
                                </div>
                              </div>
                              <div class="curved-container">
                                <h2 class="curved-text">195 / 65R15 / 91V</h2>
                                </div>
                            </div>
                         </div>
                       </div>
                      </div>
                     </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="good_yearbannr">
                      <div class="good_year_contents">
                        <img src="assets/images/goodyear.png" alt="">
                       <div class="good_yearcontents">
                        <h3>
                          Goodyear impresses in Annual <span> Auto Express Product Awards</span>
                        </h3>
                        <button>
                          Know More
                        </button>
                       </div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="step_banner">
                      <img src="assets/images/bannerstep.png" alt="">
                      <div class="round_stepdetails">
                        <div class="round_stepimg">
                          <img src="assets/images/round_steps.png" alt="">
                          <ul>
                            <li>
                              Enter Reg or size
                            </li>
                            <li>
                              Select your tyres
                            </li>
                            <li>
                              Choose a Fitter
                            </li>
                            <li>
                              Enjoy your New tyres
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="step_banner">
                      <img src="assets/images/banner5.png" alt="">
                      <div class="accelera_contents">
                        <div class="logo">
                          <img src="assets/images/logo-accelera 1.png" alt="">
                        </div>
                        <h3>
                          "Unlock Exceptional Performance! 
    Special Offer on Accelera Tyres - 
    Limitd Time Only!"
                        </h3>
                        <h6>
                          A TYRE YOU CAN RELY ON
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-pagination"></div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
              </div>
              </div>
            </div>
          </div>
        
        </div>
       
      </div>
    <div class="bestSelling_wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="best_selling_parent">
                        <div class="bestselling_contents">
                            <h2>
                                Best Selling Brands
                            </h2>
                            <p>
                                Our best selling tyre brands are here
                            </p>
                        </div>
                        <div class="bestSelling_swiperWrapper">
                            <div class="swiper  Bestselling_swiper">
                                <div class="swiper-wrapper">
                                  <div class="swiper-slide">
                                    <img src="assets/images/brand1.png" alt="">
                                  </div>
                                  <div class="swiper-slide">
                                    <img src="assets/images/brand2.png" alt="">
                                  </div>
                                  <div class="swiper-slide">
                                    <img src="assets/images/brand3.png" alt="">
                                  </div>
                                  <div class="swiper-slide">
                                    <img src="assets/images/brand4.png" alt="">
                                  </div>
                                  <div class="swiper-slide">
                                    <img src="assets/images/brand5.png" alt="">
                                  </div>
                                  <div class="swiper-slide">
                                    <img src="assets/images/brand3.png" alt="">
                                  </div>
                                  <div class="swiper-slide">
                                    <img src="assets/images/brand4.png" alt="">
                                  </div>
                                  <div class="swiper-slide">
                                    <img src="assets/images/brand5.png" alt="">
                                  </div>
                                </div>
                  
                              </div>
                              <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="brands_Listing">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="brands_listwrapper">
                        <h2>
                            Brands
                        </h2>
                        <p>
                            Select and fit your perfect and stylish popular tyres form various brands
                        </p>
                        <ul>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/bridgestone.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/goodyear_brand.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/firelli.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/michlein.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/michlein.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/goodyear_brand.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/goodyear_brand.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/goodyear_brand.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/goodyear_brand.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/goodyear_brand.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/goodyear_brand.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                            <li>
                                <div class="logo">
                                    <img src="assets/images/goodyear_brand.png" alt="">
                                </div>
                                <h3>
                                    Bridgestone
                                </h3>
                                <p>
                                    Bridgestone are a highly respected Japanese tyre manufacturer, best known for their 
production of premium tyres. They are a global 
leader in the industry, providing sustainable 
mobility and advanced solutions.
                                </p>
                                <a href="">Read More 
                                    <img src="assets/images/chevron-right.svg" alt=""> </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
