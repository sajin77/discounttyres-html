<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>

<main>
    <div class="register_page">
        <div class="banner_section">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="bannerslide_parent">
                    <div class="banner_filterBox_latest">
                      <h2>
                        order your tyre online, Fit locally
                      </h2>

                      <div class="banner_tabs">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                          <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Search Vehicle reg</button>
                          </li>
                          <li class="nav-item" role="presentation">
                            <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Search Tyre size</button>
                          </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <div class="tab_contentsBox">
                              <div class="reg_yellowWrapper">
                                <div class="reg_box">
                                  <div class="gb_blueBox">
                                    <div class="round_gb">
                                    <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle id="Ellipse 14" cx="6.5" cy="6.5" r="6" stroke="#FFCD02" stroke-dasharray="2 2"/>
                </svg>
                <h6>
                gb
                </h6>
                
                                    </div>
                                  </div>
                                  <input type="text" placeholder="ENTER REG">
                                </div>
                                <div class="fully_fittedchoose">
                                  <div class="fullyfit_select">
                                   <input type="radio" name="" id="">  Fully fitted
                                  </div>
                                  <div class="fullyfit_select">
                                    <input type="radio" name="" id=""> Mail Order
                                  </div>
                                </div>
                              </div>
                              <div class="reg_searchBtn">
                                <button>
                                  SEARCH BY REG
                                </button>
                              </div>
                            </div>
                          </div>
                          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                            <div class="tab_contentsBox">
                              <div class="tyre_sizes">
                                <select name="" id="">
                                    <option value="">width</option>
                                    <option value="">width</option>
                                    <option value="">width</option>
                                </select>
                                <select name="" id="">
                                  <option value="">Profile</option>
                                  <option value="">Profile</option>
                                </select>
                                <select name="" id="">
                                  <option value="">Wheel Size</option>
                                  <option value="">Wheel Size</option>
                                </select>
                                <select name="" id="">
                                  <option value="">Speed</option>
                                  <option value="">Speed</option>
                                </select>
                              </div>

                              <div class="fully_fitwrapper">
                                <div class="fully_fittedchoose">
                                  <div class="fullyfit_select">
                                   <input type="radio" name="" id="">  Fully fitted
                                  </div>
                                  <div class="fullyfit_select">
                                    <input type="radio" name="" id=""> Mail Order
                                  </div>
                                </div>

                                <div class="reg_searchBtn">
                                  <button>
                                    SEARCH BY REG
                                  </button>
                                </div>

                              </div>
                            </div>

                          </div>
                        
                        </div>
                      </div>
                     
                    </div>
                  <div class="swiper mySwiper banner_slider">
                    <div class="swiper-wrapper">
                      <div class="swiper-slide">
                        <div class="mrf_banner">
                         <div class="row banner_row">
                          <div class="col-12 col-md-12">
                        
                          </div>
                          <div class="col-12 col-xl-6">
                            <div class="about_mrf_tyres">
                              <div class="mrf_logo">
                                <img src="assets/images/icons/mrf.svg" alt="">
                              </div>
                              <h2>
                                Longer life Tyres
          for your Bikes
                              </h2>
                              <div class="know_moreBtn">
                                <button>
                                  Know More
                                </button>
                              </div>
                            </div>
                          </div>
                         </div>
                        </div>
                      </div>
                      <div class="swiper-slide">
                        <div class="mrf_banner2">
                          <div class="row banner_row">
                           <div class="col-12 col-md-6">
                             
                           </div>
                           <div class="col-12 col-lg-12">
                             <div class="banner_tyrespecs">
                                <div class="tyre_img">
                                  <img src="assets/images/bannertyre.png" alt="">
                                  <div class="curved_arrowgroup">
                                    <img src="assets/images/icons/arrowgroup.png" alt="">
                                    <div class="arow_tyresize left_tyre_size ">
                                      <h6>
                                        Width
                                      </h6>
                                      <h5>
                                        95
                                      </h5>
                                    </div>
        
                                    <div class="arow_tyresize top_tyre_size1">
                                      <h6>
                                        Profile
                                      </h6>
                                      <h5>
                                        195
                                      </h5>
                                    </div>
                                    <div class="arow_tyresize top_tyre_size2">
                                      <h6>
                                        Profile
                                      </h6>
                                      <h5>
                                        195
                                      </h5>
                                    </div>
        
        
                                    <div class="arow_tyresize right_tyre_size">
                                      <h6>
                                        Speed
                                      </h6>
                                      <h5>
                                        v
                                      </h5>
                                    </div>
                                  </div>
                                  <div class="curved-container">
                                    <h2 class="curved-text">195 / 65R15 / 91V</h2>
                                    </div>
                                </div>
                             </div>
                           </div>
                          </div>
                         </div>
                      </div>
                      <div class="swiper-slide">
                        <div class="good_yearbannr">
                          <div class="good_year_contents">
                            <img src="assets/images/goodyear.png" alt="">
                           <div class="good_yearcontents">
                            <h3>
                              Goodyear impresses in Annual <span> Auto Express Product Awards</span>
                            </h3>
                            <button>
                              Know More
                            </button>
                           </div>
                          </div>
                        </div>
                      </div>
                      <div class="swiper-slide">
                        <div class="step_banner">
                          <img src="assets/images/bannerstep.png" alt="">
                          <div class="round_stepdetails">
                            <div class="round_stepimg">
                              <img src="assets/images/round_steps.png" alt="">
                              <ul>
                                <li>
                                  Enter Reg or size
                                </li>
                                <li>
                                  Select your tyres
                                </li>
                                <li>
                                  Choose a Fitter
                                </li>
                                <li>
                                  Enjoy your New tyres
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="swiper-slide">
                        <div class="step_banner">
                          <img src="assets/images/banner5.png" alt="">
                          <div class="accelera_contents">
                            <div class="logo">
                              <img src="assets/images/logo-accelera 1.png" alt="">
                            </div>
                            <h3>
                              "Unlock Exceptional Performance! 
        Special Offer on Accelera Tyres - 
        Limitd Time Only!"
                            </h3>
                            <h6>
                              A TYRE YOU CAN RELY ON
                            </h6>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="swiper-pagination"></div>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                  </div>
                  </div>
                </div>
              </div>
            
            </div>
           
          </div>
        <div class="registerform">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                       
                        <h2>
                            Sign Up
                        </h2>

                        <p>
                            Fill the form to register as a fitting center
                        </p>

                        <div class="registerbox">
                            <div class="tyre">
                                <img src="assets/images/register_tyre.png" alt="">
                            </div>
                            <div class="logo">
                                <img src="assets/images/discount_tyrelogo.png" alt="">
                            </div>
                            <div class="regiter_contents">
                                <h3>
                                    Sign Up as a  Fitting Center
                                </h3>
                                <p>
                                    Enter your details to fill the form to register as a Fitting Center.
please fill in the following form and one of our representatives will be in touch soon to get you signed up.
                                </p>
                                <button>
                                    Start
                                </button>
                            </div>
                         
                        </div>

                        <div class="registerbox">
                          
                           <div class="register_companybox">
                            <div class="company_name">
                              <h3>
                                1
                              </h3>
                              <h6>
                                Company Name
                              </h6>
                            </div>
                            <div class="input_field">
                              <input type="text" placeholder="type your answer here">
                            </div>
                            <div class="button_wrapper">
                              <button class="back_btn">
                                Back
                              </button>
                              <button class="next_btn">
                                next
                              </button>
                            </div>
                           </div>
                            <div class="tyre">
                            <img src="assets/images/register_tyre.png" alt="">
                        </div>
                          </div>

                          <div class="registerbox">
                          
                            <div class="final_stage">
                              <div class="success_img">
                                <img src="assets/images/Done.png" alt="">
                              </div>
                              <h4>
                                Submitted Successfully
                              </h4>
                              <p>
                                All Done. Thanks for your Time
                              </p>
                              <p>
                                We will be in touch as soon as possible.
                              </p>
                              <div class="button_wrapper">
                                <button class="back_btn">
                                  Back
                                </button>
                              </div>
                            </div>
                           
                             <div class="tyre">
                             <img src="assets/images/register_tyre.png" alt="">
                         </div>
                           </div>
                       
                      </div>
                     
                    </div>
                </div>
            </div>
        </div>
      </div>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
