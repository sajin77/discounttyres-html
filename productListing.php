<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>

<main>
  <div class="banner_section_listing">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bannerslide_parent">
            <div class="banner_filterBox">
              <h2>
              Start by giving your register number
              </h2>
              <div class="reg_boxParent">
                <div class="reg_box">
                  <div class="gb_blueBox">
                    <div class="round_gb">
                    <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle id="Ellipse 14" cx="6.5" cy="6.5" r="6" stroke="#FFCD02" stroke-dasharray="2 2"/>
</svg>
<h6>
gb
</h6>

                    </div>
                  </div>
                  <input type="text" placeholder="ENTER REG">
                </div>
                <div class="reg_searchBtn">
                  <button>
                    SEARCH BY REG
                  </button>
                </div>
              </div>
              <div class="fully_fittedRadio_switch">
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Fully fitted</label>
                </div>
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Mail Order</label>
                </div>
              </div>
              <div class="or">
                <span>Or</span>
              </div>
              <h2>
                Check your Tyre size below
              </h2>
              <div class="tyre_sizes">
                <select name="" id="">
                    <option value="">width</option>
                    <option value="">width</option>
                    <option value="">width</option>
                </select>
                <select name="" id="">
                  <option value="">Profile</option>
                  <option value="">Profile</option>
                </select>
                <select name="" id="">
                  <option value="">Wheel Size</option>
                  <option value="">Wheel Size</option>
                </select>
                <select name="" id="">
                  <option value="">Speed</option>
                  <option value="">Speed</option>
                </select>
              </div>
              <div class="fully_fittedRadio_switch mt_19">
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Fully fitted</label>
                </div>
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Mail Order</label>
                </div>
              </div>
              <div class="continue_btn">
                <button>
                 Continue
                </button>
              </div>
            </div>
          <div class="swiper mySwiper banner_slider">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="mrf_banner">
                 <div class="row banner_row">
                  <div class="col-12 col-md-6">
                
                  </div>
                  <div class="col-12 col-md-6">
                    <div class="about_mrf_tyres">
                      <div class="mrf_logo">
                        <img src="assets/images/icons/mrf.svg" alt="">
                      </div>
                      <h2>
                        Longer life Tyres
  for your Bikes
                      </h2>
                      <div class="know_moreBtn">
                        <button>
                          Know More
                        </button>
                      </div>
                    </div>
                  </div>
                 </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="mrf_banner2">
                  <div class="row banner_row">
                   <div class="col-12 col-md-6">
                     
                   </div>
                   <div class="col-12 col-md-6">
                     <div class="about_mrf_tyres">
                       <div class="mrf_logo">
                         <img src="assets/images/icons/mrf.svg" alt="">
                       </div>
                       <h2>
                         Longer life Tyres
   for your Bikes
                       </h2>
                       <div class="know_moreBtn">
                         <button>
                           Know More
                         </button>
                       </div>
                     </div>
                   </div>
                  </div>
                 </div>
              </div>
              <div class="swiper-slide">
                <div class="mrf_banner">
                  <div class="row banner_row">
                   <div class="col-12 col-md-6">
                    
                   </div>
                   <div class="col-12 col-md-6">
                     <div class="about_mrf_tyres">
                       <div class="mrf_logo">
                         <img src="assets/images/icons/mrf.svg" alt="">
                       </div>
                       <h2>
                         Longer life Tyres
   for your Bikes
                       </h2>
                       <div class="know_moreBtn">
                         <button>
                           Know More
                         </button>
                       </div>
                     </div>
                   </div>
                  </div>
                 </div>
              </div>
              <div class="swiper-slide">
                <div class="mrf_banner2">
                  <div class="row banner_row">
                   <div class="col-12 col-md-6">
                     
                   </div>
                   <div class="col-12 col-md-6">
                     <div class="about_mrf_tyres">
                       <div class="mrf_logo">
                         <img src="assets/images/icons/mrf.svg" alt="">
                       </div>
                       <h2>
                         Longer life Tyres
   for your Bikes
                       </h2>
                       <div class="know_moreBtn">
                         <button>
                           Know More
                         </button>
                       </div>
                     </div>
                   </div>
                  </div>
                 </div>
              </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>
          </div>
        </div>
      </div>
    
    </div>
   
  </div>
  <div class="recomended_tyres_section">
    <div class="container">
        <div class="row">
            <div class="col-12">
              <h1>
                We feature these <span>tyres</span> for you
              </h1>
                <ul class="recomended_tyres">
                    <li>
                        <div class="recomended_box_head">
                            Recommended for you
                        </div>
                        <div class="recommended_body">
                            <div class="tyre">
                                <div class="tyre_brand">
                                    <img src="assets/images/recommended_brand1.png" alt="">
                                </div>
                                <div class="tyre_img">
                                    <img src="assets/images/tyre1.png" alt="">
                                </div>
                            </div>
                            <div class="about_tyre">
                                <h2>
                                    BRIDGESTONE 
Sport Contact 5
                                </h2>
                                <h3>
                                    Only <span>£101.34</span>
                                </h3>
                                <ul class="specs">
                                    <li class="fuel"> 
                                        <span>E</span>
                                        <img src="assets/images/Fuel.svg" alt="">
                                      </li>
                                      <li class="rain">
                                        <span>E</span>
                                        <img src="assets/images/Rain.svg" alt="">
                                      </li>
                                      <li class="speaker">
                                        <span>E</span>
                                        <img src="assets/images/Speaker.svg" alt="">
                                      </li>
                                </ul>
                            </div>
                        </div>
                        <div class="quntity_select">
                            <select name="" id="">
                                <option value="">20</option>
                                <option value="">20</option>
                                <option value="">20</option>
                            </select>
                            <button>
                                select
                            </button>
                        </div>
                    </li>
                    <li>
                        <div class="recomended_box_head budget_tyre">
                            Best Budget Tyre
                        </div>
                        <div class="recommended_body">
                            <div class="tyre">
                                <div class="tyre_brand">
                                    <img src="assets/images/recommended_brand1.png" alt="">
                                </div>
                                <div class="tyre_img">
                                    <img src="assets/images/tyre1.png" alt="">
                                </div>
                            </div>
                            <div class="about_tyre">
                                <h2>
                                    BRIDGESTONE 
Sport Contact 5
                                </h2>
                                <h3>
                                    Only <span>£101.34</span>
                                </h3>
                                <ul class="specs">
                                    <li class="fuel"> 
                                        <span>E</span>
                                        <img src="assets/images/Fuel.svg" alt="">
                                      </li>
                                      <li class="rain">
                                        <span>E</span>
                                        <img src="assets/images/Rain.svg" alt="">
                                      </li>
                                      <li class="speaker">
                                        <span>E</span>
                                        <img src="assets/images/Speaker.svg" alt="">
                                      </li>
                                </ul>
                            </div>
                        </div>
                        <div class="quntity_select">
                            <select name="" id="">
                                <option value="">20</option>
                                <option value="">20</option>
                                <option value="">20</option>
                            </select>
                            <button>
                                select
                            </button>
                        </div>
                    </li>
                    <li>
                        <div class="recomended_box_head popular_tyre">
                            Popular Tyre
                        </div>
                        <div class="recommended_body">
                            <div class="tyre">
                                <div class="tyre_brand">
                                    <img src="assets/images/recommended_brand1.png" alt="">
                                </div>
                                <div class="tyre_img">
                                    <img src="assets/images/tyre1.png" alt="">
                                </div>
                            </div>
                            <div class="about_tyre">
                                <h2>
                                    BRIDGESTONE 
Sport Contact 5
                                </h2>
                                <h3>
                                    Only <span>£101.34</span>
                                </h3>
                                <ul class="specs">
                                    <li class="fuel"> 
                                        <span>E</span>
                                        <img src="assets/images/Fuel.svg" alt="">
                                      </li>
                                      <li class="rain">
                                        <span>E</span>
                                        <img src="assets/images/Rain.svg" alt="">
                                      </li>
                                      <li class="speaker">
                                        <span>E</span>
                                        <img src="assets/images/Speaker.svg" alt="">
                                      </li>
                                </ul>
                            </div>
                        </div>
                        <div class="quntity_select">
                            <select name="" id="">
                                <option value="">20</option>
                                <option value="">20</option>
                                <option value="">20</option>
                            </select>
                            <button>
                                select
                            </button>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
  </div>
  <div class="filter_web">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="filter_parent">
                    <div class="filter_sidebar">
                        <div class="mobile_filter">
                            <h6>
                                Filter
                            </h6>
                            <img src="assets/images/filter.svg" alt="">
                        </div>
                        <div class="filter_head">
                            <img src="assets/images/filter.svg" alt="">
                            <h6>
                                FILTER BY
                            </h6>
                        </div>
                        <div class="filter_box">
                            <div class="close_btnmob">
                                <img src="assets/images/icons/close_btn.svg" alt="">
                            </div>
                            <div class="accordion accordion-flush" id="accordionFlushExample">
                                <div class="accordion-item">
                                  <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                        Range
                                    </button>
                                  </h2>
                                  <div id="flush-collapseOne" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <div class="item_select">
                                            <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="accordion-item">
                                  <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                        Low to High
                                    </button>
                                  </h2>
                                  <div id="flush-collapseTwo" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <div class="item_select">
                                            <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="accordion-item">
                                  <h2 class="accordion-header">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                        Brand
                                    </button>
                                  </h2>
                                  <div id="flush-collapseThree" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                    <div class="accordion-body">
                                        <div class="item_select">
                                            <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                        <div class="item_select">
                                            <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="accordion-item">
                                    <h2 class="accordion-header">
                                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapsefour" aria-expanded="false" aria-controls="flush-collapseThree">
                                        Application
                                      </button>
                                    </h2>
                                    <div id="flush-collapsefour" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                      <div class="accordion-body">
                                          <div class="item_select">
                                              <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-item">
                                    <h2 class="accordion-header">
                                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapsefive" aria-expanded="false" aria-controls="flush-collapseThree">
                                        Season
                                      </button>
                                    </h2>
                                    <div id="flush-collapsefive" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                      <div class="accordion-body">
                                          <div class="item_select">
                                              <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-item">
                                    <h2 class="accordion-header">
                                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapsesix" aria-expanded="false" aria-controls="flush-collapseThree">
                                        Fuel
                                      </button>
                                    </h2>
                                    <div id="flush-collapsesix" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                      <div class="accordion-body">
                                          <div class="item_select">
                                              <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-item">
                                    <h2 class="accordion-header">
                                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseseven" aria-expanded="false" aria-controls="flush-collapseThree">
                                          Wet
                                      </button>
                                    </h2>
                                    <div id="flush-collapseseven" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                      <div class="accordion-body">
                                          <div class="item_select">
                                              <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-item">
                                    <h2 class="accordion-header">
                                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseeight" aria-expanded="false" aria-controls="flush-collapseThree">
                                        Manufacturer Approved 
                                      </button>
                                    </h2>
                                    <div id="flush-collapseeight" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                      <div class="accordion-body">
                                          <div class="item_select">
                                              <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="accordion-item">
                                    <h2 class="accordion-header">
                                      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapsesenine" aria-expanded="false" aria-controls="flush-collapseThree">
                                      Vehicle Type
                                      </button>
                                    </h2>
                                    <div id="flush-collapsesenine" class="accordion-collapse collapse" data-bs-parent="#accordionFlushExample">
                                      <div class="accordion-body">
                                          <div class="item_select">
                                              <h6>Michelin <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Continental<span>(25)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Bridgestone <span>(08)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Yokohama <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>MRF <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Pirelli <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                          <div class="item_select">
                                              <h6>Doublestar <span>(29)</span></h6> <input type="checkbox" name="" id="">
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                              </div>
                        </div>
                    </div>
                    <div class="filter_items">
                       <div class="filter_item_ul">
                        <li data-bs-toggle="modal" data-bs-target="#listing_pop">
                            <div class="recommended_body">
                                <div class="tyre">
                                    <div class="tyre_brand">
                                        <img src="assets/images/recommended_brand1.png" alt="">
                                    </div>
                                    <div class="tyre_img">
                                        <img src="assets/images/tyre1.png" alt="">
                                    </div>
                                </div>
                                <div class="about_tyre">
                                    <h2>
                                        BRIDGESTONE 
    Sport Contact 5
                                    </h2>
                                    <h3>
                                        Only <span>£101.34</span>
                                    </h3>
                                    <ul class="specs">
                                        <li class="fuel"> 
                                            <span>E</span>
                                            <img src="assets/images/Fuel.svg" alt="">
                                          </li>
                                          <li class="rain">
                                            <span>E</span>
                                            <img src="assets/images/Rain.svg" alt="">
                                          </li>
                                          <li class="speaker">
                                            <span>E</span>
                                            <img src="assets/images/Speaker.svg" alt="">
                                          </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="recommended_body">
                                <div class="tyre">
                                    <div class="tyre_brand">
                                        <img src="assets/images/recommended_brand1.png" alt="">
                                    </div>
                                    <div class="tyre_img">
                                        <img src="assets/images/tyre2.png" alt="">
                                    </div>
                                </div>
                                <div class="about_tyre">
                                    <h2>
                                        BRIDGESTONE 
    Sport Contact 5
                                    </h2>
                                    <h3>
                                        Only <span>£101.34</span>
                                    </h3>
                                    <ul class="specs">
                                        <li class="fuel"> 
                                            <span>E</span>
                                            <img src="assets/images/Fuel.svg" alt="">
                                          </li>
                                          <li class="rain">
                                            <span>E</span>
                                            <img src="assets/images/Rain.svg" alt="">
                                          </li>
                                          <li class="speaker">
                                            <span>E</span>
                                            <img src="assets/images/Speaker.svg" alt="">
                                          </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="recommended_body">
                                <div class="tyre">
                                    <div class="tyre_brand">
                                        <img src="assets/images/recommended_brand1.png" alt="">
                                    </div>
                                    <div class="tyre_img">
                                        <img src="assets/images/tyre3.png" alt="">
                                    </div>
                                </div>
                                <div class="about_tyre">
                                    <h2>
                                        BRIDGESTONE 
    Sport Contact 5
                                    </h2>
                                    <h3>
                                        Only <span>£101.34</span>
                                    </h3>
                                    <ul class="specs">
                                        <li class="fuel"> 
                                            <span>E</span>
                                            <img src="assets/images/Fuel.svg" alt="">
                                          </li>
                                          <li class="rain">
                                            <span>E</span>
                                            <img src="assets/images/Rain.svg" alt="">
                                          </li>
                                          <li class="speaker">
                                            <span>E</span>
                                            <img src="assets/images/Speaker.svg" alt="">
                                          </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="recommended_body">
                                <div class="tyre">
                                    <div class="tyre_brand">
                                        <img src="assets/images/recommended_brand1.png" alt="">
                                    </div>
                                    <div class="tyre_img">
                                        <img src="assets/images/tyre4.png" alt="">
                                    </div>
                                </div>
                                <div class="about_tyre">
                                    <h2>
                                        BRIDGESTONE 
    Sport Contact 5
                                    </h2>
                                    <h3>
                                        Only <span>£101.34</span>
                                    </h3>
                                    <ul class="specs">
                                        <li class="fuel"> 
                                            <span>E</span>
                                            <img src="assets/images/Fuel.svg" alt="">
                                          </li>
                                          <li class="rain">
                                            <span>E</span>
                                            <img src="assets/images/Rain.svg" alt="">
                                          </li>
                                          <li class="speaker">
                                            <span>E</span>
                                            <img src="assets/images/Speaker.svg" alt="">
                                          </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="recommended_body">
                                <div class="tyre">
                                    <div class="tyre_brand">
                                        <img src="assets/images/recommended_brand1.png" alt="">
                                    </div>
                                    <div class="tyre_img">
                                        <img src="assets/images/tyre1.png" alt="">
                                    </div>
                                </div>
                                <div class="about_tyre">
                                    <h2>
                                        BRIDGESTONE 
    Sport Contact 5
                                    </h2>
                                    <h3>
                                        Only <span>£101.34</span>
                                    </h3>
                                    <ul class="specs">
                                        <li class="fuel"> 
                                            <span>E</span>
                                            <img src="assets/images/Fuel.svg" alt="">
                                          </li>
                                          <li class="rain">
                                            <span>E</span>
                                            <img src="assets/images/Rain.svg" alt="">
                                          </li>
                                          <li class="speaker">
                                            <span>E</span>
                                            <img src="assets/images/Speaker.svg" alt="">
                                          </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="recommended_body">
                                <div class="tyre">
                                    <div class="tyre_brand">
                                        <img src="assets/images/recommended_brand1.png" alt="">
                                    </div>
                                    <div class="tyre_img">
                                        <img src="assets/images/tyre1.png" alt="">
                                    </div>
                                </div>
                                <div class="about_tyre">
                                    <h2>
                                        BRIDGESTONE 
    Sport Contact 5
                                    </h2>
                                    <h3>
                                        Only <span>£101.34</span>
                                    </h3>
                                    <ul class="specs">
                                        <li class="fuel"> 
                                            <span>E</span>
                                            <img src="assets/images/Fuel.svg" alt="">
                                          </li>
                                          <li class="rain">
                                            <span>E</span>
                                            <img src="assets/images/Rain.svg" alt="">
                                          </li>
                                          <li class="speaker">
                                            <span>E</span>
                                            <img src="assets/images/Speaker.svg" alt="">
                                          </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="recommended_body">
                                <div class="tyre">
                                    <div class="tyre_brand">
                                        <img src="assets/images/recommended_brand1.png" alt="">
                                    </div>
                                    <div class="tyre_img">
                                        <img src="assets/images/tyre1.png" alt="">
                                    </div>
                                </div>
                                <div class="about_tyre">
                                    <h2>
                                        BRIDGESTONE 
    Sport Contact 5
                                    </h2>
                                    <h3>
                                        Only <span>£101.34</span>
                                    </h3>
                                    <ul class="specs">
                                        <li class="fuel"> 
                                            <span>E</span>
                                            <img src="assets/images/Fuel.svg" alt="">
                                          </li>
                                          <li class="rain">
                                            <span>E</span>
                                            <img src="assets/images/Rain.svg" alt="">
                                          </li>
                                          <li class="speaker">
                                            <span>E</span>
                                            <img src="assets/images/Speaker.svg" alt="">
                                          </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="recommended_body">
                                <div class="tyre">
                                    <div class="tyre_brand">
                                        <img src="assets/images/recommended_brand1.png" alt="">
                                    </div>
                                    <div class="tyre_img">
                                        <img src="assets/images/tyre1.png" alt="">
                                    </div>
                                </div>
                                <div class="about_tyre">
                                    <h2>
                                        BRIDGESTONE 
    Sport Contact 5
                                    </h2>
                                    <h3>
                                        Only <span>£101.34</span>
                                    </h3>
                                    <ul class="specs">
                                        <li class="fuel"> 
                                            <span>E</span>
                                            <img src="assets/images/Fuel.svg" alt="">
                                          </li>
                                          <li class="rain">
                                            <span>E</span>
                                            <img src="assets/images/Rain.svg" alt="">
                                          </li>
                                          <li class="speaker">
                                            <span>E</span>
                                            <img src="assets/images/Speaker.svg" alt="">
                                          </li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="productList_pagination">
    <nav aria-label="Page navigation example">
      <ul class="pagination">
          <li class="page-item">
          <a class="page-link pagination_prev" href="#" aria-label="Previous">
              <span aria-hidden="true"></span>
          </a>
          </li>
          <li class="page-item"><a class="page-link active" href="#">1</a></li>
          <li class="page-item"><a class="page-link" href="#">2</a></li>
          <li class="page-item"><a class="page-link" href="#">...</a></li>
          <li class="page-item"><a class="page-link" href="#">9</a></li>
          <li class="page-item"><a class="page-link" href="#">10</a></li>
          <li class="page-item">
          <a class="page-link pagination_next" href="#" aria-label="Next">
              <span aria-hidden="true"></span>
          </a>
          </li>
      </ul>
      </nav>
    </div>
</main>

<!-- Button trigger modal -->


<!-- Modal -->
<div class="modal fade listing_popup" id="listing_pop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="staticBackdropLabel">We are almost there,you just need to check your order and enter your postcode.</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="pop_card">
          <div class="tyre_detailParent">
            <div class="recommended_body">
              <div class="tyre">
                 
                  <div class="tyre_img">
                      <img src="assets/images/tyre1.png" alt="">
                  </div>
              </div>
              <div class="about_tyre">
                <div class="tyre_brand">
                  <img src="assets/images/recommended_brand1.png" alt="">
              </div>
                  <h2>
                      BRIDGESTONE 
Sport Contact 5
                  </h2>
                  <h3>
                      Only <span>£101.34</span>
                  </h3>
                  <ul class="specs">
                      <li class="fuel"> 
                          <span>E</span>
                          <img src="assets/images/Fuel.svg" alt="">
                        </li>
                        <li class="rain">
                          <span>E</span>
                          <img src="assets/images/Rain.svg" alt="">
                        </li>
                        <li class="speaker">
                          <span>E</span>
                          <img src="assets/images/Speaker.svg" alt="">
                        </li>
                  </ul>
              </div>
          </div>

          <div class="price_details">
            <div class="closebtn">
              <img src="assets/images/icons/cardclose.svg" alt="">
            </div>
            <h3>
              £98.34
            </h3>
            <div class="quntity_select">
              <select name="" id="">
                  <option value="">20</option>
                  <option value="">20</option>
                  <option value="">20</option>
              </select>
          </div>
          </div>

          </div>
        </div>
        <div class="pop_card">
          <div class="tyre_detailParent">
            <div class="recommended_body">
              <div class="tyre">
                 
                  <div class="tyre_img">
                      <img src="assets/images/tyre1.png" alt="">
                  </div>
              </div>
              <div class="about_tyre">
                <div class="tyre_brand">
                  <img src="assets/images/recommended_brand1.png" alt="">
              </div>
                  <h2>
                      BRIDGESTONE 
Sport Contact 5
                  </h2>
                  <h3>
                      Only <span>£101.34</span>
                  </h3>
                  <ul class="specs">
                      <li class="fuel"> 
                          <span>E</span>
                          <img src="assets/images/Fuel.svg" alt="">
                        </li>
                        <li class="rain">
                          <span>E</span>
                          <img src="assets/images/Rain.svg" alt="">
                        </li>
                        <li class="speaker">
                          <span>E</span>
                          <img src="assets/images/Speaker.svg" alt="">
                        </li>
                  </ul>
              </div>
          </div>

          <div class="price_details">
            <div class="closebtn">
              <img src="assets/images/icons/cardclose.svg" alt="">
            </div>
            <h3>
              £98.34
            </h3>
            <div class="quntity_select">
              <select name="" id="">
                  <option value="">20</option>
                  <option value="">20</option>
                  <option value="">20</option>
              </select>
          </div>
          </div>

          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <div class="search_fields">
          <div class="input_parent">
              <input type="text" placeholder="Enter Your Post Code...">
          </div>
          <button class="go">
            Choose a Fitter
          </button>
      </div>
      </div>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade listing_popup" id="listing_pop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="staticBackdropLabel">We are almost there,you just need to check your order and enter your postcode.</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="pop_card">
          <div class="tyre_detailParent">
            <div class="recommended_body">
              <div class="tyre">
                 
                  <div class="tyre_img">
                      <img src="assets/images/tyre1.png" alt="">
                  </div>
              </div>
              <div class="about_tyre">
                <div class="tyre_brand">
                  <img src="assets/images/recommended_brand1.png" alt="">
              </div>
                  <h2>
                      BRIDGESTONE 
Sport Contact 5
                  </h2>
                  <h3>
                      Only <span>£101.34</span>
                  </h3>
                  <ul class="specs">
                      <li class="fuel"> 
                          <span>E</span>
                          <img src="assets/images/Fuel.svg" alt="">
                        </li>
                        <li class="rain">
                          <span>E</span>
                          <img src="assets/images/Rain.svg" alt="">
                        </li>
                        <li class="speaker">
                          <span>E</span>
                          <img src="assets/images/Speaker.svg" alt="">
                        </li>
                  </ul>
              </div>
          </div>

          <div class="price_details">
            <div class="closebtn">
              <img src="assets/images/icons/cardclose.svg" alt="">
            </div>
            <h3>
              £98.34
            </h3>
            <div class="quntity_select">
              <select name="" id="">
                  <option value="">20</option>
                  <option value="">20</option>
                  <option value="">20</option>
              </select>
          </div>
          </div>

          </div>
        </div>
        <div class="pop_card">
          <div class="tyre_detailParent">
            <div class="recommended_body">
              <div class="tyre">
                 
                  <div class="tyre_img">
                      <img src="assets/images/tyre1.png" alt="">
                  </div>
              </div>
              <div class="about_tyre">
                <div class="tyre_brand">
                  <img src="assets/images/recommended_brand1.png" alt="">
              </div>
                  <h2>
                      BRIDGESTONE 
Sport Contact 5
                  </h2>
                  <h3>
                      Only <span>£101.34</span>
                  </h3>
                  <ul class="specs">
                      <li class="fuel"> 
                          <span>E</span>
                          <img src="assets/images/Fuel.svg" alt="">
                        </li>
                        <li class="rain">
                          <span>E</span>
                          <img src="assets/images/Rain.svg" alt="">
                        </li>
                        <li class="speaker">
                          <span>E</span>
                          <img src="assets/images/Speaker.svg" alt="">
                        </li>
                  </ul>
              </div>
          </div>

          <div class="price_details">
            <div class="closebtn">
              <img src="assets/images/icons/cardclose.svg" alt="">
            </div>
            <h3>
              £98.34
            </h3>
            <div class="quntity_select">
              <select name="" id="">
                  <option value="">20</option>
                  <option value="">20</option>
                  <option value="">20</option>
              </select>
          </div>
          </div>

          </div>
        </div>
        
      </div>
      <div class="modal-footer">
        <div class="search_fields">
          <div class="input_parent">
              <input type="text" placeholder="Enter Your Post Code...">
          </div>
          <button class="go">
            Choose a Fitter
          </button>
      </div>
      </div>
    </div>
  </div>
</div>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
