<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>

<main>
  <div class="banner_section">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="bannerslide_parent">
            <div class="banner_filterBox">
              <h2>
                Start by giving your register number
              </h2>
              <div class="reg_boxParent">
                <div class="reg_box">
                  <div class="gb_blueBox">
                    <div class="round_gb">
                    <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
<circle id="Ellipse 14" cx="6.5" cy="6.5" r="6" stroke="#FFCD02" stroke-dasharray="2 2"/>
</svg>
<h6>
gb
</h6>

                    </div>
                  </div>
                  <input type="text" placeholder="ENTER REG">
                </div>
                <div class="reg_searchBtn">
                  <button>
                    SEARCH BY REG
                  </button>
                </div>
              </div>
              <div class="fully_fittedRadio_switch">
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Fully fitted</label>
                </div>
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Mail Order</label>
                </div>
              </div>
              <div class="or">
                <span>Or</span>
              </div>
              <h2>
                Check your Tyre size below
              </h2>
              <div class="tyre_sizes">
                <select name="" id="">
                    <option value="">width</option>
                    <option value="">width</option>
                    <option value="">width</option>
                </select>
                <select name="" id="">
                  <option value="">Profile</option>
                  <option value="">Profile</option>
                </select>
                <select name="" id="">
                  <option value="">Wheel Size</option>
                  <option value="">Wheel Size</option>
                </select>
                <select name="" id="">
                  <option value="">Speed</option>
                  <option value="">Speed</option>
                </select>
              </div>
              <div class="fully_fittedRadio_switch mt_19">
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Fully fitted</label>
                </div>
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Mail Order</label>
                </div>
              </div>
              <div class="continue_btn">
                <button>
                 Continue
                </button>
              </div>
            </div>
          <div class="swiper mySwiper banner_slider">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="mrf_banner">
                 <div class="row banner_row">
                  <div class="col-12 col-md-12">
                
                  </div>
                  <div class="col-12 col-xl-6">
                    <div class="about_mrf_tyres">
                      <div class="mrf_logo">
                        <img src="assets/images/icons/mrf.svg" alt="">
                      </div>
                      <h2>
                        Longer life Tyres
  for your Bikes
                      </h2>
                      <div class="know_moreBtn">
                        <button>
                          Know More
                        </button>
                      </div>
                    </div>
                  </div>
                 </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="mrf_banner2">
                  <div class="row banner_row">
                   <div class="col-12 col-md-6">
                     
                   </div>
                   <div class="col-12 col-lg-12">
                     <div class="banner_tyrespecs">
                        <div class="tyre_img">
                          <img src="assets/images/bannertyre.png" alt="">
                          <div class="curved_arrowgroup">
                            <img src="assets/images/icons/arrowgroup.png" alt="">
                            <div class="arow_tyresize left_tyre_size ">
                              <h6>
                                Width
                              </h6>
                              <h5>
                                95
                              </h5>
                            </div>

                            <div class="arow_tyresize top_tyre_size1">
                              <h6>
                                Profile
                              </h6>
                              <h5>
                                195
                              </h5>
                            </div>
                            <div class="arow_tyresize top_tyre_size2">
                              <h6>
                                Profile
                              </h6>
                              <h5>
                                195
                              </h5>
                            </div>


                            <div class="arow_tyresize right_tyre_size">
                              <h6>
                                Speed
                              </h6>
                              <h5>
                                v
                              </h5>
                            </div>
                          </div>
                          <div class="curved-container">
                            <h2 class="curved-text">195 / 65R15 / 91V</h2>
                            </div>
                        </div>
                     </div>
                   </div>
                  </div>
                 </div>
              </div>
              <div class="swiper-slide">
                <div class="good_yearbannr">
                  <div class="good_year_contents">
                    <img src="assets/images/goodyear.png" alt="">
                   <div class="good_yearcontents">
                    <h3>
                      Goodyear impresses in Annual <span> Auto Express Product Awards</span>
                    </h3>
                    <button>
                      Know More
                    </button>
                   </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="step_banner">
                  <img src="assets/images/bannerstep.png" alt="">
                  <div class="round_stepdetails">
                    <div class="round_stepimg">
                      <img src="assets/images/round_steps.png" alt="">
                      <ul>
                        <li>
                          Enter Reg or size
                        </li>
                        <li>
                          Select your tyres
                        </li>
                        <li>
                          Choose a Fitter
                        </li>
                        <li>
                          Enjoy your New tyres
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="swiper-slide">
                <div class="step_banner">
                  <img src="assets/images/banner5.png" alt="">
                  <div class="accelera_contents">
                    <div class="logo">
                      <img src="assets/images/logo-accelera 1.png" alt="">
                    </div>
                    <h3>
                      "Unlock Exceptional Performance! 
Special Offer on Accelera Tyres - 
Limitd Time Only!"
                    </h3>
                    <h6>
                      A TYRE YOU CAN RELY ON
                    </h6>
                  </div>
                </div>
              </div>
            </div>
            <div class="swiper-pagination"></div>
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
          </div>
          </div>
        </div>
      </div>
    
    </div>
   
  </div>
  <div class="banner_ul">
    <div class="container">
      <div class="row experice_ulParent">
        <div class="col-12">
          <ul class="experiece">
            <li data-bs-toggle="modal" data-bs-target="#choosetyre_pop">
              <img src="assets/images/icons/Timer.svg" alt="">
              SAME DAY FITTING 
            </li>
            <li>
              <img src="assets/images/icons/Reward.svg" alt="">
              25+ YEARS SELLING TYRES ONLINE
            </li>
            <li>
              <img src="assets/images/icons/delivery.svg" alt="">
              MOBILE FITTING SERVICE
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="order_tyreSection">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="order_contents">
            <h2>
              Order <span>Tyres</span> online in 4 Easy Steps
            </h2>

            <ul class="steps">
              <li>
                <img src="assets/images/step1.png" alt="">
              </li>
              <li>
                <img src="assets/images/step2.png" alt="">
              </li>
              <li>
                <img src="assets/images/step3.png" alt="">
              </li>
              <li>
                <img src="assets/images/step4.png" alt="">
              </li>
            </ul>

          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="branded_tyreSection">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="order_contents">
            <h2>
              Our average savings across premium <span>branded tyres</span>
            </h2>

           <div class="swiper_brandsparentt">
           <div class="swiper mySwiper branded_tyreSlider">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <img src="assets/images/brand1.png" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/images/brand2.png" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/images/brand3.png" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/images/brand4.png" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/images/brand5.png" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/images/brand3.png" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/images/brand4.png" alt="">
                </div>
                <div class="swiper-slide">
                  <img src="assets/images/brand5.png" alt="">
                </div>
              </div>

            </div>
            <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
           </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tyre_budgets">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="tyre_budget_details">
            <h2>
              <span>Tyres</span> to suit all budgets
            </h2>
            <p>
              Best Tyres online tyre store is the best place to come for your new tyres. We have premium branded tyres, fantastic performing mid-range brands and a wide selection of cheap car tyres from our budget range. We supply tyres from leading brands like Bridgestone, 
and many more, so you will be spoilt for choice. Don’t worry about how you’ll get your tyres fitted either, etyres have been providing convenient mobile tyre fitting for over 25 years.
            </p>
            <ul class="budget_ul">
              <li>
                <div class="budget_img">
                  <img src="assets/images/budget1.png" alt="">
                  
                </div>
                <div class="hovering_box">
                  <div class="main_head">
                    <h2>
                      Premium Tyres
                    </h2>
                    <div class="arrow">
                      <img src="assets/images/chevron-down.svg" alt="">
                    </div>
                  </div>
                  <p>
                    Besttyres stocks a large range of premium tyres from big brands, including Bridgestone, Continental, Dunlop, Goodyear, Michelin and Pirelli.
                  </p>
                </div>
              </li>
              <li>
                <div class="budget_img">
                  <img src="assets/images/budget2.png" alt="">
                 
                </div>
                <div class="hovering_box">
                  <div class="main_head">
                    <h2>
                      Mid-range Tyres
                    </h2>
                    <div class="arrow">
                      <img src="assets/images/chevron-down.svg" alt="">
                    </div>
                  </div>
                  <p>
                    Besttyres stocks a large range of premium tyres from big brands, including Bridgestone, Continental, Dunlop, Goodyear, Michelin and Pirelli.
                  </p>
                </div>
              </li>
              <li>
                <div class="budget_img">
                  <img src="assets/images/budget3.png" alt="">
                  
                </div>
                <div class="hovering_box">
                  <div class="main_head">
                    <h2>
                      Budget Tyres
                    </h2>
                    <div class="arrow">
                      <img src="assets/images/chevron-down.svg" alt="">
                    </div>
                  </div>
                  <p>
                    Besttyres stocks a large range of premium tyres from big brands, including Bridgestone, Continental, Dunlop, Goodyear, Michelin and Pirelli.
                  </p>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="tyre_banner">
   <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="tyrebanner_contents">
          <h2>
            Get your favourite Tyres 
      with Special Discount  
          </h2>
          <button>
            View Tyres
          </button>
        </div>
      </div>
    </div>
   </div>
  </div>
  <div class="tyre_list">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2>
            Our popular <span>car tyre</span> ranges
          </h2>
          <ul class="tyrelisting_ul">
            <li><a href="">
              <div class="product_img">
                <img src="assets/images/tyre1.png" alt="">
              </div>
              <div class="tyre_details">
                <h3>
                  Continental Sport Contact
                </h3>
                <h5>
                  Only <span> £98.34</span>
                </h5>
                <ul class="tyre_features">
                  <li class="fuel"> 
                    <span>E</span>
                    <img src="assets/images/Fuel.svg" alt="">
                  </li>
                  <li class="rain">
                    <span>E</span>
                    <img src="assets/images/Rain.svg" alt="">
                  </li>
                  <li class="speaker">
                    <span>E</span>
                    <img src="assets/images/Speaker.svg" alt="">
                  </li>
                </ul>
              </div>
            </a></li>
            <li><a href="">
              <div class="product_img">
                <img src="assets/images/tyre1.png" alt="">
              </div>
              <div class="tyre_details">
                <h3>
                  Continental Sport Contact
                </h3>
                <h5>
                  Only <span> £98.34</span>
                </h5>
                <ul class="tyre_features">
                  <li class="fuel"> 
                    <span>E</span>
                    <img src="assets/images/Fuel.svg" alt="">
                  </li>
                  <li class="rain">
                    <span>E</span>
                    <img src="assets/images/Rain.svg" alt="">
                  </li>
                  <li class="speaker">
                    <span>E</span>
                    <img src="assets/images/Speaker.svg" alt="">
                  </li>
                </ul>
              </div>
            </a></li>
            <li><a href="">
              <div class="product_img">
                <img src="assets/images/tyre1.png" alt="">
              </div>
              <div class="tyre_details">
                <h3>
                  Continental Sport Contact
                </h3>
                <h5>
                  Only <span> £98.34</span>
                </h5>
                <ul class="tyre_features">
                  <li class="fuel"> 
                    <span>E</span>
                    <img src="assets/images/Fuel.svg" alt="">
                  </li>
                  <li class="rain">
                    <span>E</span>
                    <img src="assets/images/Rain.svg" alt="">
                  </li>
                  <li class="speaker">
                    <span>E</span>
                    <img src="assets/images/Speaker.svg" alt="">
                  </li>
                </ul>
              </div>
            </a></li>
            <li><a href="">
              <div class="product_img">
                <img src="assets/images/tyre1.png" alt="">
              </div>
              <div class="tyre_details">
                <h3>
                  Continental Sport Contact
                </h3>
                <h5>
                  Only <span> £98.34</span>
                </h5>
                <ul class="tyre_features">
                  <li class="fuel"> 
                    <span>E</span>
                    <img src="assets/images/Fuel.svg" alt="">
                  </li>
                  <li class="rain">
                    <span>E</span>
                    <img src="assets/images/Rain.svg" alt="">
                  </li>
                  <li class="speaker">
                    <span>E</span>
                    <img src="assets/images/Speaker.svg" alt="">
                  </li>
                </ul>
              </div>
            </a></li>
            <li><a href="">
              <div class="product_img">
                <img src="assets/images/tyre1.png" alt="">
              </div>
              <div class="tyre_details">
                <h3>
                  Continental Sport Contact
                </h3>
                <h5>
                  Only <span> £98.34</span>
                </h5>
                <ul class="tyre_features">
                  <li class="fuel"> 
                    <span>E</span>
                    <img src="assets/images/Fuel.svg" alt="">
                  </li>
                  <li class="rain">
                    <span>E</span>
                    <img src="assets/images/Rain.svg" alt="">
                  </li>
                  <li class="speaker">
                    <span>E</span>
                    <img src="assets/images/Speaker.svg" alt="">
                  </li>
                </ul>
              </div>
            </a></li>
            <li><a href="">
              <div class="product_img">
                <img src="assets/images/tyre1.png" alt="">
              </div>
              <div class="tyre_details">
                <h3>
                  Continental Sport Contact
                </h3>
                <h5>
                  Only <span> £98.34</span>
                </h5>
                <ul class="tyre_features">
                  <li class="fuel"> 
                    <span>E</span>
                    <img src="assets/images/Fuel.svg" alt="">
                  </li>
                  <li class="rain">
                    <span>E</span>
                    <img src="assets/images/Rain.svg" alt="">
                  </li>
                  <li class="speaker">
                    <span>E</span>
                    <img src="assets/images/Speaker.svg" alt="">
                  </li>
                </ul>
              </div>
            </a></li>
            <li><a href="">
              <div class="product_img">
                <img src="assets/images/tyre1.png" alt="">
              </div>
              <div class="tyre_details">
                <h3>
                  Continental Sport Contact
                </h3>
                <h5>
                  Only <span> £98.34</span>
                </h5>
                <ul class="tyre_features">
                  <li class="fuel"> 
                    <span>E</span>
                    <img src="assets/images/Fuel.svg" alt="">
                  </li>
                  <li class="rain">
                    <span>E</span>
                    <img src="assets/images/Rain.svg" alt="">
                  </li>
                  <li class="speaker">
                    <span>E</span>
                    <img src="assets/images/Speaker.svg" alt="">
                  </li>
                </ul>
              </div>
            </a></li>
            <li class="view_allTyres">
              <a href="" class="view_allBtn">
                View All
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="shop_tyre">
   <div class="container">
    <div class="row">
      <div class="col-12">
       <div class="tyre_shopingDetails">
        <div class="tyre_types">
          <h2>
            Shop <span>Tyre</span> Type
          </h2>
        </div>
        <div class="climte_suitable_sliderparent">
          <div class="swiper mySwiper climte_suitable_slider">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <a href="">
                  <div class="tyre_slideImg">
                    <img src="assets/images/icons/shop_tyre1.png" alt="">
                    <span>
                      All Season Tyres
                    </span>
                  </div>
                </a>
              </div>
              <div class="swiper-slide">
               <a href="">
                <div class="tyre_slideImg">
                  <img src="assets/images/icons/shop_tyre2.png" alt="">
                  <span>
                    Run Flat Tyres
                  </span>
                </div>
               </a>
              </div>
              <div class="swiper-slide">
                <a href="">
                  <div class="tyre_slideImg">
                    <img src="assets/images/icons/shop_tyre3.png" alt="">
                    <span>
                      Budget Car Tyres
                    </span>
                  </div>
                </a>
              </div>
              <div class="swiper-slide">
                <a href="">
                  <div class="tyre_slideImg">
                    <img src="assets/images/icons/shop_tyre1.png" alt="">
                    <span>
                      All Season Tyres
                    </span>
                  </div>
                </a>
              </div>
              <div class="swiper-slide">
               <a href="">
                <div class="tyre_slideImg">
                  <img src="assets/images/icons/shop_tyre2.png" alt="">
                  <span>
                    Run Flat Tyres
                  </span>
                </div>
               </a>
              </div>
            </div>
          </div>
        </div>
       </div>
      </div>
    </div>
   </div>
  </div>


  <!-- Modal -->
<div class="modal fade choosetyre_popup" id="choosetyre_pop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="pop_card">
          <div class="tyrebrand_img">
            <img src="assets/images/eagle_tyrebrand.png" alt="">
          </div>
          <div class="tyrebrand_details">
            <h2>
              We’ve found tyres for your VAUXHALL CORSA SRI NAV PREMIUIM TURBO
            </h2>
            <h6>
              Please select your tyre size to get started
            </h6>
            <div class="whats_nextBox">
              <div class="alertbox">
                <img src="assets/images/alert.svg" alt="">
                <h6>
                  What's Next
                </h6>
              </div>
              <p>
                Once you have confirmed the correct tyres for your vehicle we’ll show you some amazing deals
              </p>
            </div>
          </div>
        </div>

        <div class="tyre_codedetails">
          <h3>
            Your Tyres
          </h3>
          <p>
            These are several options for your tyres, have a look at the current tyres and match the numbers.
          </p>
          <div class="tyresize_box">
            <img src="assets/images/check.png" alt=""> 195/55R16 87H
          </div>
          <h4>
            Can’t find your tyres, <a href=""> Click here</a> to search by Size
          </h4>


          <div class="tyresize_detailedBox">
            <div class="head">
              <h2>
                Can’t Find a match for your vehicle?
              </h2>
              <p>
                Search for your exact tyre size instead using our tyre search.
              </p>
            </div>
            <div class="size_details">
              <div class="tyre_sizes">
                <div class="select_parent">
                  <label for="">
                    Width
                  </label>
                  <select name="" id="">
                    <option value="">205</option>
                    <option value="">210</option>
                    <option value="">205</option>
                    <option value="">205</option>
                    <option value="">210</option>
                    <option value="">205</option>
                </select>
                </div>
                <div class="select_parent">
                  <label for="">
                    Profile
                  </label>
                  <select name="" id="">
                    <option value="">60</option>
                    <option value="">205</option>
                    <option value="">210</option>
                    <option value="">205</option>
                    <option value="">90</option>
                    <option value="">50</option>
                </select>
                </div>
                <div class="select_parent">
                  <label for="">
                    Wheel Size
                  </label>
                  <select name="" id="">
                    <option value="">10</option>
                    <option value="">205</option>
                    <option value="">210</option>
                    <option value="">205</option>
                    <option value="">10</option>
                    <option value="">10</option>
                </select>
                </div>
                <div class="select_parent">
                  <label for="">
                    speed
                  </label>
                  <select name="" id="">
                    <option value="">50</option>
                    <option value="">205</option>
                    <option value="">210</option>
                    <option value="">205</option>
                    <option value="">60</option>
                    <option value="">70</option>
                </select>
                </div>
              </div>
              <div class="fully_fittedRadio_switch mt_19">
                <div class="radio_switch">
                  <input type="radio" name="fully_fitted" id="">
                  <label for="">Fully fitted</label>
                </div>

              </div>
              <div class="continue_btn">
                <button>
                 Continue
                </button>
              </div>
            </div>
          </div>

        </div>
        
      </div>
      <div class="modal-footer">
  
      </div>
    </div>
  </div>
</div>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
