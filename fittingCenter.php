<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>

<main>
    <section class="fitting_centers">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="fitting_centerResults">
                        <h2 class="results">
                            Showing Local Filters in the area of M11AD
                        </h2>
                        <div class="search_fields">
                            <div class="input_parent">
                                <input type="text">
                            </div>
                            <button class="go">
                                Go
                            </button>
                        </div>
                    </div>
                    <ul class="service_centers">
                        <li><a href="#">
                            <div class="service_centerImg">
                                <img src="assets/images/serviceCenter1.png" alt="">
                            </div>
                            <div class="service_details">
                                <div class="location">
                                    <h2>
                                        MDR Autos Centres
                                    </h2>
                                    <button>
                                        View Location
                                    </button>
                                </div>
                                <p>
                                    114B Manchester Road
ASHTON-UNDER-LYNE Lancashire OL5 9AY
                                </p>
                                <div class="distance">
                                    <img src="assets/images/icons/distance.svg" alt="">
                                    <span>9.1 Miles From You</span>
                                </div>
                                <div class="date_time">
                                    <input type="date" name="" id="">
                                   <select name="" id="">
                                    <option value="">9.00 am</option>
                                    <option value="">9.00 am</option>
                                    <option value="">9.00 am</option>
                                   </select>
                                </div>
                                <button class="book_btn">
                                    Book Your Fitting
                                </button>
                            </div>
                        </a></li>
                        <li><a href="">
                            <div class="service_centerImg">
                                <img src="assets/images/serviceCenter1.png" alt="">
                            </div>
                            <div class="service_details">
                                <div class="location">
                                    <h2>
                                        MDR Autos Centres
                                    </h2>
                                    <button>
                                        View Location
                                    </button>
                                </div>
                                <p>
                                    114B Manchester Road
ASHTON-UNDER-LYNE Lancashire OL5 9AY
                                </p>
                                <div class="distance">
                                    <img src="assets/images/icons/distance.svg" alt="">
                                    <span>9.1 Miles From You</span>
                                </div>
                                <div class="date_time">
                                    <input type="date" name="" id="">
                                    <input type="time" name="" id="">
                                </div>
                                <button class="book_btn">
                                    Book Your Fitting
                                </button>
                            </div>
                        </a></li>
                        <li><a href="">
                            <div class="service_centerImg">
                                <img src="assets/images/serviceCenter1.png" alt="">
                            </div>
                            <div class="service_details">
                                <div class="location">
                                    <h2>
                                        MDR Autos Centres
                                    </h2>
                                    <button>
                                        View Location
                                    </button>
                                </div>
                                <p>
                                    114B Manchester Road
ASHTON-UNDER-LYNE Lancashire OL5 9AY
                                </p>
                                <div class="distance">
                                    <img src="assets/images/icons/distance.svg" alt="">
                                    <span>9.1 Miles From You</span>
                                </div>
                                <div class="date_time">
                                    <input type="date" name="" id="">
                                    <input type="time" name="" id="">
                                </div>
                                <button class="book_btn">
                                    Book Your Fitting
                                </button>
                            </div>
                        </a></li>
                        <li><a href="">
                            <div class="service_centerImg">
                                <img src="assets/images/serviceCenter1.png" alt="">
                            </div>
                            <div class="service_details">
                                <div class="location">
                                    <h2>
                                        MDR Autos Centres
                                    </h2>
                                    <button>
                                        View Location
                                    </button>
                                </div>
                                <p>
                                    114B Manchester Road
ASHTON-UNDER-LYNE Lancashire OL5 9AY
                                </p>
                                <div class="distance">
                                    <img src="assets/images/icons/distance.svg" alt="">
                                    <span>9.1 Miles From You</span>
                                </div>
                                <div class="date_time">
                                    <input type="date" name="" id="">
                                    <input type="time" name="" id="">
                                </div>
                                <button class="book_btn">
                                    Book Your Fitting
                                </button>
                            </div>
                        </a></li>
                        <li><a href="">
                            <div class="service_centerImg">
                                <img src="assets/images/serviceCenter1.png" alt="">
                            </div>
                            <div class="service_details">
                                <div class="location">
                                    <h2>
                                        MDR Autos Centres
                                    </h2>
                                    <button>
                                        View Location
                                    </button>
                                </div>
                                <p>
                                    114B Manchester Road
ASHTON-UNDER-LYNE Lancashire OL5 9AY
                                </p>
                                <div class="distance">
                                    <img src="assets/images/icons/distance.svg" alt="">
                                    <span>9.1 Miles From You</span>
                                </div>
                                <div class="date_time">
                                    <input type="date" name="" id="">
                                    <input type="time" name="" id="">
                                </div>
                                <button class="book_btn">
                                    Book Your Fitting
                                </button>
                            </div>
                        </a></li>
                        <li><a href="">
                            <div class="service_centerImg">
                                <img src="assets/images/serviceCenter1.png" alt="">
                            </div>
                            <div class="service_details">
                                <div class="location">
                                    <h2>
                                        MDR Autos Centres
                                    </h2>
                                    <button>
                                        View Location
                                    </button>
                                </div>
                                <p>
                                    114B Manchester Road
ASHTON-UNDER-LYNE Lancashire OL5 9AY
                                </p>
                                <div class="distance">
                                    <img src="assets/images/icons/distance.svg" alt="">
                                    <span>9.1 Miles From You</span>
                                </div>
                                <div class="date_time">
                                    <input type="date" name="" id="">
                                    <input type="time" name="" id="">
                                </div>
                                <button class="book_btn">
                                    Book Your Fitting
                                </button>
                            </div>
                        </a></li>
                    </ul>
                    <div class="load_morebtn">
                        <button>
                            load
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
