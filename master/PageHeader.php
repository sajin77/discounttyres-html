<header>
  <div class="header">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="header_contents">
            <div class="logo">
              <a href="">
                <img src="./assets/images/icons/logo.svg" alt="">
              </a>
            </div>
            <div class="office_openTime">
              <img src="./assets/images/icons/clock.svg" alt="">
              <span>Office Open 9am - 6pm</span>
            </div>
            <div class="cart_profile">
              <ul>
                <li><a href="">
                  <img src="./assets/images/icons/profile.svg" alt="">
                </a></li>
                <li><a href="">
                  <img src="./assets/images/icons/cart.svg" alt="">
                </a></li>
                <li class="menu_toggle"><a href="#">
                  <img src="./assets/images/icons/Menu.svg" alt="">
                </a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="menu_header">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="nav_headerContents">
            <div class="nav">
            <div class="close_btnmob">
                                <img src="assets/images/icons/close_btn.svg" alt="">
                            </div>
              <ul>
                <li><a href="">
                  Home
                </a></li>
                <li><a href="about.php">
                  About us
                </a></li>
                <li><a href="">
                  Tyres <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 7.5L10 12.5L15 7.5" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                    
                </a></li>
                <li><a href="">
                  Brands <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5 7.5L10 12.5L15 7.5" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>                    
                </a></li>
                <li><a href="">
                  Offers
                </a></li>
                <li><a href="contact.php">
                  Conatct Us
                </a></li>
              </ul>
            </div>
            <div class="office_openTimeResponsive">
              <img src="./assets/images/icons/clock.svg" alt="">
              <span>Office Open 9am - 6pm</span>
            </div>
            <div class="contact">
              <img src="./assets/images/icons/Phone.svg" alt="">
              <p>For sales & advice call: <a href="">0800 652 3120</a> </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
