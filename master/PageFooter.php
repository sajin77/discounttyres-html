<footer>
    <div class="main_footer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="footer_contents">
                        <div class="logo_section">
                            <div class="logo">
                                <a href="">
                                    <img src="./assets/images/icons/logo.svg" alt="">
                                </a>
                            </div>
                            <div class="location">
                                <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M22.0713 20.8211L16.7675 26.1249C16.5356 26.3571 16.2601 26.5412 15.957 26.6669C15.6538 26.7926 15.3288 26.8573 15.0006 26.8573C14.6724 26.8573 14.3475 26.7926 14.0443 26.6669C13.7411 26.5412 13.4657 26.3571 13.2338 26.1249L7.92875 20.8211C6.53028 19.4226 5.57792 17.6408 5.19211 15.701C4.8063 13.7612 5.00437 11.7505 5.76126 9.92329C6.51815 8.09606 7.79988 6.5343 9.44436 5.43551C11.0888 4.33672 13.0222 3.75024 15 3.75024C16.9778 3.75024 18.9112 4.33672 20.5557 5.43551C22.2001 6.5343 23.4819 8.09606 24.2388 9.92329C24.9956 11.7505 25.1937 13.7612 24.8079 15.701C24.4221 17.6408 23.4697 19.4226 22.0713 20.8211Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    <path d="M18.75 13.75C18.75 14.7446 18.3549 15.6984 17.6517 16.4017C16.9484 17.1049 15.9946 17.5 15 17.5C14.0054 17.5 13.0516 17.1049 12.3483 16.4017C11.6451 15.6984 11.25 14.7446 11.25 13.75C11.25 12.7554 11.6451 11.8016 12.3483 11.0983C13.0516 10.3951 14.0054 10 15 10C15.9946 10 16.9484 10.3951 17.6517 11.0983C18.3549 11.8016 18.75 12.7554 18.75 13.75Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                    </svg>                                    
                                <a href="">
                                    Address Line 1 United Kingdom
                                </a>
                            </div>
                        </div>
                        <div class="footer_menu">
                            <div class="quick_links">
                                <h2>
                                    Quick Links
                                </h2>
                                <div class="menu_flex">
                                    <ul>
                                        <li><a href="">
                                            Home
                                        </a></li>
                                        <li><a href="">
                                            About us
                                        </a></li>
                                        <li><a href="">
                                            Contact us
                                        </a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="">
                                            Tyres <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5 7.5L10 12.5L15 7.5" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>
                                        </a></li>
                                        <li><a href="">
                                            Brands <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5 7.5L10 12.5L15 7.5" stroke="black" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                                </svg>
                                        </a></li>
                                        <li><a href="">
                                            Offers
                                        </a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="quick_links">
                                <h2>
                                    Give us a call on
                                </h2>
                                <ul>
                                    <li><a href="">
                                        0800 652 3120
                                    </a></li>
                                </ul>
                            </div>
                            <div class="quick_links">
                                <h2>
                                    Send us an email
                                </h2>
                                <ul>
                                    <li><a href="">
                                        enquiries@tyresavings.com
                                    </a></li>
                                </ul>
                            </div>
                            <div class="quick_links">
                                <h2>
                                    connect with us
                                </h2>
                                <ul class="social_media">
                                    <li>
                                        <a href="">
                                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M13 0.550049C6.125 0.550049 0.5 6.16255 0.5 13.075C0.5 19.3251 5.075 24.5125 11.05 25.45V16.7H7.875V13.075H11.05V10.3125C11.05 7.17505 12.9125 5.45005 15.775 5.45005C17.1375 5.45005 18.5625 5.68755 18.5625 5.68755V8.77505H16.9875C15.4375 8.77505 14.95 9.73755 14.95 10.725V13.075H18.425L17.8625 16.7H14.95V25.45C17.8956 24.9849 20.5778 23.4819 22.5124 21.2126C24.4471 18.9433 25.5067 16.0571 25.5 13.075C25.5 6.16255 19.875 0.550049 13 0.550049Z" fill="white"/>
                                                </svg>                                                
                                        </a>
                                    </li>
                                    <li>
                                        <a href="">
                                            <svg width="28" height="22" viewBox="0 0 28 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M27.075 2.875C26.1125 3.3125 25.075 3.6 24 3.7375C25.1 3.075 25.95 2.025 26.35 0.7625C25.3125 1.3875 24.1625 1.825 22.95 2.075C21.9625 1 20.575 0.375 19 0.375C16.0625 0.375 13.6625 2.775 13.6625 5.7375C13.6625 6.1625 13.7125 6.575 13.8 6.9625C9.34999 6.7375 5.38749 4.6 2.74999 1.3625C2.28749 2.15 2.02499 3.075 2.02499 4.05C2.02499 5.9125 2.96249 7.5625 4.41249 8.5C3.52499 8.5 2.69999 8.25 1.97499 7.875C1.97499 7.875 1.97499 7.875 1.97499 7.9125C1.97499 10.5125 3.82499 12.6875 6.27499 13.175C5.82499 13.3 5.34999 13.3625 4.86249 13.3625C4.52499 13.3625 4.18749 13.325 3.86249 13.2625C4.53749 15.375 6.49999 16.95 8.86249 16.9875C7.03749 18.4375 4.72499 19.2875 2.19999 19.2875C1.77499 19.2875 1.34999 19.2625 0.924988 19.2125C3.29999 20.7375 6.12499 21.625 9.14999 21.625C19 21.625 24.4125 13.45 24.4125 6.3625C24.4125 6.125 24.4125 5.9 24.4 5.6625C25.45 4.9125 26.35 3.9625 27.075 2.875Z" fill="white"/>
                                                </svg>
                                                                                             
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub_footer">
       <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="sub_footercontents">
                    <p>
                        © 2023 BESTTYRES. All Rights Reserved
                    </p>
                    <ul>
                        <li><a href="">
                            Privacy Policy
                        </a></li>
                        <li><a href="">
                            Terms & Conditions
                        </a></li>
                    </ul>
                   </div>
            </div>
        </div>
       </div>
    </div>
</footer>