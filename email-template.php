<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>


<style>
    /* body {
        font-family: 'Arial', sans-serif;
        background-color: #f4f4f4;
        margin: 0;
        padding: 0;
    }

    .container {
        max-width: 600px;
        margin: 0 auto;
        padding: 20px;
        background-color: #ffffff;
        border-radius: 10px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
    } */

    .template_outer {
        padding: 20px 0;
        padding-bottom: 50px;
        /* max-width: 1000px;
        width: 100%;
        margin: auto; */
    }

    h2 {
        text-align: center;
    }

    p {
        color: #555;
        line-height: 1.6;
        font-size: 14px;
        margin-top: 10px;
    }

    table {
        width: 100%;
        border-collapse: collapse;
        margin-top: 20px;
        background-color: #f9f9f9;
        border-radius: 8px;
    }

    th, td {
        border: 1px solid #ddd;
        padding: 15px;
        text-align: left;
        font-size: 14px;
    }

    tfoot td {
        text-align: right;
        font-size: 14px;
        font-weight: bold;
    }

    tfoot td:first-child {
        border: none;
    }

    .footer {
        margin-top: 20px;
        text-align: center;
        color: #021639;
        font-weight: 500;
        font-size: 20px;
    }
    .template_head .logo {
        display: flex;
        justify-content: center;
        margin-bottom: 10px;

    }
    .template_head .logo img {
        max-width: 150px;
        width: 100%;
    }

    @media (max-width: 575.98px) {
        .tyre_item {
            width: 80px;
            height: 80px;
        }
        .table_parent {
            overflow-x: auto;
            width: 100%;
        }
        .template_outer {
            padding: 0;
            padding-bottom: 50px;
        }
    }
 
</style>




<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="template_outer">

                <div class="template_head" style="background-color: #021639; padding: 20px 20px; ">
                    <div class="logo">
                        <img src="assets/images/icons/logo.svg" alt="">
                    </div>
                    <h2 style="color: white;">Thanks For Shopping With Us </h2>
                </div>
            
              
            
                <p>Dear jijo,</p>
            
                <p>Thank you for choosing us! Your order has been received and is now being processed. Here are the details:</p>
            
                <div class="table_parent">
                    <table>
                        <thead>
                            <tr style="background-color: #021639; color: #fff;">
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>tyre</th>
                                <th style="text-align: right;">Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr>
                                <td>Mrf Tyres</td>
                                <td>2</td>
                                <td>
                                    <img class="tyre_item" style="width: 140px; height: 140px;" src="assets/images/tyre1.png" alt="">
                                </td>
                                <td style="text-align: right;">10000</td>
                            </tr>
                             
                            <tr>
                                <td>Mrf Tyres</td>
                                <td>2</td>
                                <td>
                                    <img class="tyre_item" style="width: 140px; height: 140px;" src="assets/images/tyre1.png" alt="">
                                </td>
                                <td style="text-align: right;">10000</td>
                            </tr>
                          
                        </tbody>
                        <tfoot>
                            <tr>
                                <td style="text-align: right;" colspan="3">Total</td>
                                <td>5000</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            
                <p>Your order will be shipped to the following address:</p>
                <p>unknown address</p>
            
                <p>If you have any questions or concerns, feel free to reply to this email or contact our customer support at discounttyres.com</p>
            
                <p class="footer">Thank you for choosing Discount Tyres</p>
            
            </div>
        </div>
    </div>
</div>





<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
