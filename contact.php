<?php include "master/header.php" ?>
<?php include 'master/PageHeader.php'; ?>


<main>
    <div class="contact_page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact_details">
                        <h2>
                            <span>Contact</span> Us
                        </h2>
                        <p>
                            Get in touch with us and let us know how we can help you. For any queries regarding your tyres or our service, Contact us via the contact form below.
                        </p>
                        <div class="contact_form_address">
                            <div class="contact_address">
                                <div class="location">
                                    <div class="location_img">
                                        <img src="assets/images/icons/Location_contact.svg" alt="">
                                    </div>
                                    <div class="location_name">
                                        <h3>
                                            Contact Address
                                        </h3>
                                        <p>
                                            Address Line 1
United Kingdom
                                        </p>
                                    </div>
                                </div>
                                <div class="location">
                                    <div class="location_img">
                                        <img src="assets/images/icons/Phone_contact.svg" alt="">
                                    </div>
                                    <div class="location_name">
                                        <h3>
                                           Phone
                                        </h3>
                                        <a href="">
                                            0800 652 3120
                                        </a>
                                    </div>
                                </div>
                                <div class="location">
                                    <div class="location_img">
                                        <img src="assets/images/icons/Mail_contact.svg" alt="">
                                    </div>
                                    <div class="location_name">
                                        <h3>
                                            E-MAIL
                                        </h3>
                                        <a href="">
                                            enquiries@discounttyres.com
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="contact_form">
                                <form action="">
                                    <div class="input_parent">
                                        <input type="text" placeholder="Name">
                                    </div>
                                    <div class="input_parent">
                                        <input type="text" placeholder="E-Mail">
                                    </div>
                                    <div class="input_parent">
                                        <input type="text" placeholder="Subject">
                                    </div>
                                    <div class="input_parent">
                                       <textarea name="Message" id="" cols="30" rows="3" placeholder="Message">

                                       </textarea>
                                    </div>
                                    <div class="input_btn">
                                        <button>
                                            Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
        <div class="contactwith_us">
            <h4>
                Connect with us
            </h4>
            <ul>
                <li><a href="">
                    <img src="assets/images/facebook.svg" alt="">
                </a></li>
                <li><a href="">
                    <img src="assets/images/twitter.svg" alt="">
                </a></li>
            </ul>
        </div>
    </div>
</main>

<?php include "master/Footer.php" ?>
<?php include 'master/PageFooter.php'; ?>
