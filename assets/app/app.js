var Bestselling_slider = new Swiper(".Bestselling_swiper", {
  slidesPerView: 3,
  loop: true,
  spaceBetween: 30,
  speed:1000,
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
  autoplay: {
    delay: 2500,
    disableOnInteraction: false,
  },
  breakpoints: {
    320: {
      slidesPerView: 2.1,
      spaceBetween: 30,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 30,
    },
    992: {
      slidesPerView: 1.8,
      spaceBetween: 15,
    },
    1100: {
      slidesPerView: 2.3,
      spaceBetween: 20,
    },
    1250: {
      slidesPerView: 3,
      spaceBetween: 30,
    },
  },
});

  // niceselect js

  $("select").niceSelect();

  //bannerSlider

  var bannerSlider = new Swiper(".banner_slider", {
    grabCursor: true,
    loop: true,
    speed:1000,
    pagination: {
      el: "#Banner_pagination",
      clickable: true,
    },
    // autoplay: {
    //   delay: 2500,
    //   disableOnInteraction: false,
    // },
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    
  });


  //curved

  var str = $('.curved-text').html();
	var curved = '';
	for (var i = 0, len = str.length; i < len; i++) {
		curved += '<span class="char';
		curved += i;
		curved += '">';
		curved += str[i];
		curved += '</span>';
	}
	$('.curved-text').html(curved);



  var testimonialswiper = new Swiper(".testimonialswiper", {
    grabCursor: true,
    speed:1000,
    slidesPerView: "1.5",
    spaceBetween: 60,
    loop: true,
    centeredSlides: true, 
    pagination: {
      el: ".swiper-pagination",
      clickable:true,
    },
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    breakpoints: {
      320: {
        spaceBetween: 20,
      },
      768: {
        spaceBetween: 60,
      },
    },
  });



      //counter


      var counters = $(".count");
      var countersQuantity = counters.length;
      var counter = [];
    
      for (i = 0; i < countersQuantity; i++) {
        counter[i] = parseInt(counters[i].innerHTML);
      }
    
      var count = function(start, value, id) {
        var localStart = start;
        setInterval(function() {
          if (localStart < value) {
            localStart++;
            counters[id].innerHTML = localStart;
          }
        }, 10);
      }
    
      for (j = 0; j < countersQuantity; j++) {
        count(0, counter[j], j);
      }
  
          //counter


          $(".mobile_filter").click(function () {
            // $(this).addClass("price_limitShow");
            $(".filter_box").toggleClass("filter_boxshow");
            $("body").toggleClass("body_disable");
          });

          $(".close_btnmob").click(function () {
            // $(this).addClass("price_limitShow");
            $(".filter_box").removeClass("filter_boxshow");
            $("body").removeClass("body_disable");
          });


          $(".menu_toggle").click(function () {
            // $(this).addClass("price_limitShow");
            $(".nav").toggleClass("nav_open");
            $("body").toggleClass("body_disable");
          });

          $(".close_btnmob").click(function () {
            // $(this).addClass("price_limitShow");
            $(".nav").removeClass("nav_open");
            $("body").removeClass("body_disable");
          });
      








  var branded_tyreSlider = new Swiper(".branded_tyreSlider", {
    slidesPerView: 5,
    loop: true,
    spaceBetween: 42,
    speed:1000,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    autoplay: {
      delay: 2500,
      disableOnInteraction: false,
    },
    breakpoints: {
      320: {
        slidesPerView: 2.5,
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 3,
        spaceBetween: 40,
      },
      1024: {
        slidesPerView: 4,
        spaceBetween: 50,
      },
      1200: {
        slidesPerView: 5,
        spaceBetween: 42,
      },
    },
  });

  // var branded_tyreSlider = new Swiper(".climte_suitable_slider", {
  //   slidesPerView: 3,
  //   loop: true,
  //   speed:1000,
  //   spaceBetween: 20,
  //   navigation: {
  //     nextEl: ".swiper-button-next",
  //     prevEl: ".swiper-button-prev",
  //   },
  //   autoplay: {
  //     delay: 2500,
  //     disableOnInteraction: false,
  //   },
  //   breakpoints: {
  //     320: {
  //       slidesPerView: 2,
  //       spaceBetween: 20,
  //     },
  //     768: {
  //       slidesPerView: 3,
  //       spaceBetween: 20,
  //     },
  //     1024: {
  //       slidesPerView: 3,
  //       spaceBetween: 20,
  //     },
  //   },
  // });
